<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="pl.sdacademy.Employee" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: RENT
  Date: 2017-10-09
  Time: 18:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!--
    język wyrażeń
    -->
    <title>Wszyscy pracownicy</title>
</head>
<body>
<h2>Lista wszystkich pracowników z językiem wyrażeń</h2>

<ol>
    <c:if test="${not empty employees}">
        <c:forEach items="${employees}" var="employee">
            <li>

                ${employee.firstName} ${employee.lastName},
                ${employee.email}, ${employee.phoneNumber}

            </li>
    </c:forEach>
    </c:if>
    <c:if test="${empty employees}">
        Lista pracowników jest pusta
    </c:if>
    <!--
    inna pętla(~)
    -->

    <c:forEach var="index" begin="1" end="10">
        ${index} <br>
    </c:forEach>

</ol>
</body>
</html>
