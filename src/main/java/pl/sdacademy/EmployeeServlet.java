package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * http://dominisz.pl
 * 23.09.2017
 */
@WebServlet(urlPatterns = {"/showAllEmployees", "/addEmployee"})
public class EmployeeServlet extends HttpServlet {

    @Resource(name = "jdbc/hr_database")
    private DataSource dataSource;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestUri = req.getRequestURI();
        if (requestUri.endsWith("/addEmployee")) {
            addEmployee(req, resp);
        }
    }

    private void addEmployee(HttpServletRequest req, HttpServletResponse resp) {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String jobId = req.getParameter("jobId");
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO employees(FIRST_NAME, LAST_NAME, EMAIL, HIRE_DATE, JOB_ID) VALUES (?, ?, ?, ?, ?);");
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, email);
            preparedStatement.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setString(5, jobId);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestUri = req.getRequestURI();
        if (requestUri.endsWith("/showAllEmployees")) {
            showAllEmployees(req, resp);
        }
    }

    private void showAllEmployees(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        //to jest po to, zeby z bazy danych wyswietlalo co chcemy w intelliJ
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select first_name, last_name, email, phone_number from employees");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Employee> employees = new LinkedList<>();
            while (resultSet.next()) {
                String firstName = resultSet.getString(1);
                String lastName = resultSet.getString(2);
                String email = resultSet.getString(3);
                String phoneNumber = resultSet.getString(4);
                Employee employee = new Employee(firstName, lastName, email, phoneNumber);
                employees.add(employee);
            }
            //trzeba wszystko zamknac
            resultSet.close();
            preparedStatement.close();
            connection.close();
            //-- architektura mvc,
            req.setAttribute("employees", employees);
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("showAllEmployees.jsp");
            requestDispatcher.forward(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
