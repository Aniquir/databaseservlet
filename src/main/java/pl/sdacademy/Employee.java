package pl.sdacademy;

import com.oracle.webservices.internal.api.databinding.DatabindingMode;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by RENT on 2017-10-09.
 */
@Data
@AllArgsConstructor
public class Employee {

    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
}
